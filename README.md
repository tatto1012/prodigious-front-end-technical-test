# Prodigious Front-End Technical Test #

Technical test by Jonathan Rodriguez

### Require ###
* npm
* bower
* gulp

### Set-Up ###

1. run the npm command line for download the dependencies

```
#!console

npm install
```

2. run the bower comman-line for download the dependencies
```
#!console

bower install
```

3. run the gulp comman-line for build the app resources
```
#!console

gulp build
```

4. run the gulp comman-line for launch the app in the chrome browser in case that doesn't open the browser you can access to the project [http://localhost:8080](http://localhost:8080)

```
#!console

gulp webServer
```


### Result : ###

The reasons why I chose these widgets is that they are the most demanding in the PSD, in terms of functionality, integration and widget styles.

The widgets selected are:
	* Blog Post
	* Contact Form
	* Profile
	* Statistics

For the management of I18N, were created files .json configured to handle multiple languages, keeping key reference for the objects and language file using a simple and structured configuration.

Ajax is used for loading files .json which simulate the call to a web request, they were used for the text and graphics of widgets.

Highcharts library was used to generate the graphs.
Font-awesome library was used to replace the assets of images and to have control of the style of each icon.
The responsive page is made with bootstrap grid.
Profile numbers were encouraged to striking the widget.
The form is validated with regular expressions and giving feedback enteneder usability for the user operation.
The save function of the form is stored in local storage, the content is filled again on refresh browser action, deselect the option to save the form will be the reset.
It was used ngTagsInput and random color for field contact form, email formatting placing in red text inserted if that does not match a valid format is valid.




### Solucion : ###

Las razones por las que yo escogí estos widgets es porque son los widget más exigentes dentro del PSD, en cuanto a funcionalidad, integración y estilos.

Los widgets escogidos fueron los siguientes:
	* Blog Post
	* Contact Form
	* Profile
	* Statistics

Para el manejo del I18N, fueron creados los archivos .json configurados para manejar multiples idiomas, manteniendo la llave de referencia de los objectos por archivo y por idioma haciendo uso de una configuracion sencilla y estructurada.

Se utiliza ajax para el cargue de los archivos .json los cuales simulan el llamado a una petición web, se utilizaron para los textos y gráficas de los widgets.
Se utilizó la librería de highcharts para generar las gráficas.
El responsive de la página esta hecho con grid bootstrap.
Se animaron los números del profile para hacer llamativo el widget.
El formulario esta validado con expresiones regulares y dando el feedback de usabilidad para hacer enteneder al usuario su funcionamiento.
La funcion de save del formulario es guardada en local storage y cuando se refresca la pagina el contenido es rellenado de nuevo, al deseleccionar la opcion del save el formulario sera reseteado.
Se utilizo ngTagsInput y randomColor para el campo contactos del formulario, se valida el formato de email colocando en rojo el texto insertado en caso que no coincida con un formato valido.