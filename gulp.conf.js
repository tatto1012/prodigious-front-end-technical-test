var buildDirs = {
  js  : 'assets/js/',
  css : 'assets/css/'
};

var targetName = {
  js: 'app.js',
  vendorJs: 'vendor.js',
  css : 'styles'
};

var jsScripts = [
  'application/scripts/app.js',
	'application/scripts/**/*.js',
	'application/scripts/**/**/*.js'
];

var vendorScripts =[
  'node_modules/angular/angular.min.js',
  'node_modules/lodash/index.js',
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/moment/moment.js',
  'bower_components/bootstrap/dist/js/bootstrap.min.js',
  'bower_components/ng-tags-input/ng-tags-input.js',
  //'bower_components/highstock-release/highstock-all.js',
  'bower_components/highcharts-release/highcharts.js',
  'bower_components/highstock-release/highcharts-3d.js',
  'bower_components/highcharts-ng/dist/highcharts-ng.js',
  'bower_components/jquery-animateNumber/jquery.animateNumber.js',
  'bower_components/randomColor/randomColor.js',
];

var lessAndCssFiles = [
  'application/styles/less/app.less',
  'bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.less',
  'bower_components/ng-tags-input/ng-tags-input.css'
];

var lessWatchFiles =[
  'application/styles/less/app.less',
  'application/styles/less/**/*.less',
  'application/styles/less/**/**/*.less'
];

module.exports = {
  buildDirs      : buildDirs,
  targetName     : targetName,
  jsScripts      : jsScripts,
  vendorScripts  : vendorScripts,
  lessAndCssFiles: lessAndCssFiles,
  lessWatchFiles : lessWatchFiles
};