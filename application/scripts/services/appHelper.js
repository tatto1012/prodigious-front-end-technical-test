'use strict';

app.constant('appHelper', {

  templatesDir: 'application/templates',

  templatePath: function(view_name){
    return this.templatesDir + '/' + view_name + '.html';
  }

});