'use strict';

app.factory('LanguageService', function ($http, $location) {

  return {
    getLanguageList : getLanguageList,
    getHeaderLanguageValues : getHeaderLanguageValues,
    getGenericLanguageValues : getGenericLanguageValues,
    getFooterLanguageValues : getFooterLanguageValues,
    getWidgetBlogPostData : getWidgetBlogPostData,
    getWidgetProfileData : getWidgetProfileData,
    getWidgetContactFormData : getWidgetContactFormData,
    getWidgetStatisticsData : getWidgetStatisticsData
  };

  function getLanguageList(callback){
    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/availableLanguages.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });
  };

  function getHeaderLanguageValues(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/header.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getGenericLanguageValues(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/generic.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getFooterLanguageValues(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/footer.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetBlogPostData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetBlogPost.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetProfileData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetProfile.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetStatisticsData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetStatistics.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetContactFormData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetContactForm.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

});
