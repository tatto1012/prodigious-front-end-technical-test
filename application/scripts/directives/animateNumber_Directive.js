/**
 * animateNumber
 * @author:         Jonathan Rodriguez
 */
app.directive( 'animateNumber', function(appHelper){

  return {

    restrict: 'E',
    replace: true,
    template : '<span id="followers-counter"></span>',
    link : function(scope, element, attr){

      var decimalPlaces = (attr.decimalPlaces && attr.decimalPlaces != '')? attr.decimalPlaces : 2;
    	var initValue = (attr.initialValue && attr.initialValue != '')? attr.initialValue : 0;
    	var endValue  = (attr.endValue && attr.endValue != '')? attr.endValue : 10;

      if(decimalPlaces == '0'){
        $(element).prop('number', initValue)
        .animateNumber({ number: endValue }, 5000 );
      }
      else{
      	$(element).prop('number', initValue).animateNumber({ number: endValue, numberStep: function(now, tween) {

          var target = $(tween.elem);

          if (decimalPlaces > 0) {
            now = now.toFixed(decimalPlaces);
            now = now.toString().replace('.', ',');
          }

          target.text(now);

        }}, 3000);
      }


    }

  };

});