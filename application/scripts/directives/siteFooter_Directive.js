/**
 * siteFooter
 * @author:         Jonathan Rodriguez
 */
app.directive( 'siteFooter', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'SiteFooterCtrl',
    templateUrl: appHelper.templatePath('layout/footer'),
    scope:{

    }

  }

});