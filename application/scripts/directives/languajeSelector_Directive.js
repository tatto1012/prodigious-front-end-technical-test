/**
 * languageSelector
 * @author:         Jonathan Rodriguez
 */
app.directive( 'languageSelector', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'LanguageSelectorCtrl',
    templateUrl: appHelper.templatePath('header-content/language-selector'),
    scope:{
      currentLanguage : "=currentLanguage"
    }

  }

});