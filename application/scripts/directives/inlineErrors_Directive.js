app.directive( 'inlineError', function(appHelper){

  	return {

    	restrict: 'E',
    	templateUrl: appHelper.templatePath('features/inline-errors'),
    	scope:{
    		errors: '=errors'
    	}

  	}

});