/**
 * widgetBlogPost
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetBlogPost', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetBlogPostCtrl',
    templateUrl: appHelper.templatePath('widgets/blogPost'),

  }

});