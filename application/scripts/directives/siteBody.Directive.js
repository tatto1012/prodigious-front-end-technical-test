/**
 * siteBody
 * @author:         Jonathan Rodriguez
 */
app.directive( 'siteBody', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'SiteBodyCtrl',
    templateUrl: appHelper.templatePath('layout/body'),
    scope:{

    }

  }

});