/**
 * siteHeader
 * @author:         Jonathan Rodriguez
 */
app.directive( 'siteHeader', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'SiteHeaderCtrl',
    templateUrl: appHelper.templatePath('layout/header'),
    scope:{
      currentLanguage : "=currentLanguage"
    }

  }

});