/**
 * widgetContactForm
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetContactForm', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetContactFormCtrl',
    templateUrl: appHelper.templatePath('widgets/contactForm'),
    scope:{

    }

  }

});