/**
 * widgetProfile
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetProfile', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetProfileCtrl',
    templateUrl: appHelper.templatePath('widgets/profile'),
    scope:{

    }

  }

});