/**
 * widgetStatistics
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetStatistics', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetStatisticsCtrl',
    templateUrl: appHelper.templatePath('widgets/statistics'),
    scope:{

    }

  }

});