app.controller('WidgetBlogPostCtrl', function(LanguageService, $rootScope, $scope) {

	$scope.widgetData = $rootScope.widgetBlogPostText;

	$scope.$watch('$root.widgetBlogPostText', function(newValue){
    $scope.widgetData = newValue;
  },true);

});