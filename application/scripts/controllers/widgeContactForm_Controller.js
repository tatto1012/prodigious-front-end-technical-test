app.controller('WidgetContactFormCtrl', function(LanguageService, $rootScope, $scope, $timeout, $window) {

	$scope.widgetData = $rootScope.widgetContactFormText.form;

  $scope.$watch('$root.widgetContactFormText', function(newValue){
    $scope.widgetData = newValue.form;
  },true);

	$scope.errors = {};
	$scope.showStatusMessage = false;
	$scope.saveFormstatus = false;
	$scope.notifyStyle = "danger";
	$scope.notifyMesage = "";
	$scope.contactForm = {
		subject : '',
		message : ''
	};
	$scope.emails  = [
		'jhon@doe.com',
		'jhon+1@doe.com'
	];


  $scope.setColoursLabels = function(){
  	_.each($(".tagsInputsStyles .tags .tag-item"), function(item,key){
    	var color = randomColor();
    	if( !$(item).hasClass('colourApplied') ){
    		$(item).css({ "background" : color, "border-color" : color}).addClass('colourApplied');
    	}
    });
  };


  $scope.sendEmail = function(){
  	$scope.showStatusMessage = false;

  	if(!$scope.emails || _.isEmpty($scope.emails) || $scope.emails.length == 0){
  		$scope.errors['email'] = $scope.widgetData.errorCases.email;
  	}

  	if(!$scope.contactForm.subject.$valid){
  		$scope.errors['subject'] = $scope.widgetData.errorCases.subject;
  	}

  	if(!$scope.contactForm.message.$valid){
  		$scope.errors['message'] = $scope.widgetData.errorCases.message;
  	}

  	if($scope.contactForm.$valid){
  		$scope.showStatusMessage = true;
  		$scope.notifyStyle = "success";
  		$scope.notifyMesage = $scope.widgetData.messages.success;

  		$timeout(function(){
  			$scope.resetForm();
  		},2000);
  	}
  	else{
  		$scope.showStatusMessage = true;
  		$scope.notifyStyle = "danger";
  		$scope.notifyMesage = $scope.widgetData.messages.error;
  	}

  };

  $scope.resetError = function(item){
  	$scope.errors[item] = false;
  	$scope.showStatusMessage = false;
  };

  $scope.resetForm = function(){
  	$scope.showStatusMessage = false;
  	$scope.contactForm = {
			subject : '',
			subjectValue : '',
			message : '',
			messageValue : ''
		};
		$scope.emails = [];
  };

  $scope.saveStatus = function(){
  	$scope.showStatusMessage = true;
  	if($scope.saveFormstatus){
  		var contactFormValues = {
  			emails : _.pluck($scope.emails,'text'),
  			subjectValue : $scope.contactForm.subject['$modelValue'],
  			messageValue : $scope.contactForm.message['$modelValue']
  		};
  		$window.localStorage.setItem("formSaved", JSON.stringify(contactFormValues));
  		$scope.notifyStyle = "info";
  		$scope.notifyMesage = $scope.widgetData.messages.info;

  		$timeout(function(){
  			$scope.showStatusMessage = false;
  		},2000);
  	}
  	else{
  		$scope.notifyStyle = "warning";
  		$scope.notifyMesage = $scope.widgetData.messages.warning;

  		$window.localStorage.removeItem("formSaved");

  		$timeout(function(){
  			$scope.resetForm();
  			$scope.showStatusMessage = false;
  		},2000);
  	}
  };

  $scope.refillContactForm = function(){
  	if($window.localStorage.getItem("formSaved")){
  		var formData = JSON.parse($window.localStorage.getItem("formSaved"));
  		$scope.saveFormstatus = true;

  		_.each(formData, function(itemValue, key){
  			if(key == 'emails'){
  				$scope[key] = itemValue;
  			}
  			else{
  				$scope.contactForm[key] = itemValue;
  			}
  		});
  	}
  	else{
  		$scope.saveFormstatus = false;
  	}
  }

  $scope.$watch('emails.length', function(value) {
    $scope.resetError('email');
    $scope.setColoursLabels();
  });

  $timeout(function(){
  	$scope.setColoursLabels();
  	$scope.refillContactForm();
  }, 1000);

});