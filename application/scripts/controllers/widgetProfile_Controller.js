app.controller('WidgetProfileCtrl', function(LanguageService, $rootScope, $scope) {

	$scope.widgetData = $rootScope.widgetProfileText;

	$scope.$watch('$root.widgetProfileText', function(newValue){
    $scope.widgetData = newValue;
  },true);

	$scope.highlightToogle= function(item, status){
		var color = (status)? '#e55f3b' : '#c6bfb8' ;
		$(".fa-"+item).css({ 'color' : color });

		if(status){
			$(".fa-"+item).closest('.btn').addClass('separator');
		}
		else{
			$(".fa-"+item).closest('.btn').removeClass('separator');
		}
	}

});