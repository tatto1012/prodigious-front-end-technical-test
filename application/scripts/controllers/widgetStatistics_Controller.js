app.controller('WidgetStatisticsCtrl', function(LanguageService, $rootScope, $scope, $timeout) {

	$scope.showDiagramStats  = true;
	$scope.showMonthlyReport = false;

  $scope.widgetData = $rootScope.widgetStatisticsText;

  $scope.$watch('$root.widgetStatisticsText', function(newValue){
    $scope.widgetData = newValue;
    $scope.setChartsData();
  },true);

  $scope.showTab = function(tabItem){
    var tabs = ['showDiagramStats', 'showMonthlyReport'];

    _.each(tabs, function(item, key){
      $scope[item] = false;
    });

    switch(tabItem){
      case 'showDiagramStats':
          $scope[tabItem] = true;
        break;
      case 'showMonthlyReport':
          $scope[tabItem] = true;
        break;
    };

  };

  $scope.DTchartConfig = {
    options : {
      plotOptions: {
        pie: {
          innerSize : 130,
          allowPointSelect: true,
          cursor: 'pointer',
          showInLegend: true,
          slicedOffset: 0,
          depth : 45

        }
      },
      legend: {
        enabled : false
      }
    },
    title: {
      text: ''
    },
    series: [{
      type: 'pie',
      name: '',
      data: [],
      dataLabels: {
        enabled: false
      }
    }],
    tooltip : {
      enabled :false
    },
    func: function chartMiddlevalues(chart) {

      var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
      var textY = chart.plotTop  + (chart.plotHeight * 0.5);

      var filesAmount = ($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[0])? $scope.widgetData.charts[0].filesAmount : '-';
      var filesGB = ($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[0])? $scope.widgetData.charts[0].filesGB : '-';

      var content = '<span id="pieChartInfoText" style="position:absolute; text-align:center;width:85px;">';
      content += '<span style="font-size:16px;color:red;font-weight:bold;">'+ filesAmount +'</span><br>';
      content += '<span style="font-size: 33px;color:gray;font-weight:bold;">'+ filesGB +'</span>';
      content += '</span>';

      $("#addText").append(content);
      var span = $('#pieChartInfoText');
      $('#pieChartInfoText').css({
        'left' : textX + (span.width() * -0.3),
        'top'  : textY + (span.height() * -0.5)
      });

    }

  };


  $scope.MDTconfig = {
    title: {
      text: ''
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: { text: '' }
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
      borderWidth: 0
    },
    series: []
  };

  $scope.setChartsData = function(){

    if($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[0]){
      $scope.DTchartConfig.title.text = $scope.widgetData.charts[0].title;
      $scope.DTchartConfig.series[0].name = $scope.widgetData.charts[0].label;
      $scope.DTchartConfig.series[0].data = $scope.widgetData.charts[0].data;
    }

    if($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[1]){
      $scope.MDTconfig.title.text = $scope.widgetData.charts[1].title;
      $scope.MDTconfig.categories = $scope.widgetData.charts[1].categories;
      $scope.MDTconfig.yAxis.title.text = $scope.widgetData.charts[1].label;
      $scope.MDTconfig.series = $scope.widgetData.charts[1].data
    }

  }

  $timeout(function() {
    $scope.setChartsData();
  }, 1000);


});