app.controller('LanguageSelectorCtrl', function(LanguageService, $scope) {

	$scope.languages = {};

  $scope.getLanguageList = function(){
  	LanguageService.getLanguageList(function(err, data){
  		if(!err){
  			$scope.languages = data.languages[$scope.currentLanguage];
  		}
  		else{
  			console.error("Unable to get the language list");
  		}
  	});
  };

  $scope.$watch('currentLanguage', function(newValue, OldValue){
    $scope.getLanguageList();
  });

});