app.controller('mainCtrl', function(LanguageService, $rootScope, $scope) {

  $rootScope.headerText  = {};
	$rootScope.footerText  = {};
  $rootScope.genericText = {};
  $rootScope.widgetBlogPostText     = {};
  $rootScope.widgetProfileText      = {};
  $rootScope.widgetStatisticsText   = {};
  $rootScope.widgetContactFormText  = {};

  $scope.setLanguage = function(language){
    $rootScope.currentLanguage = language;

    $scope.setHeaderText(language);
  	$scope.setGenericText(language);
    $scope.setFooterText(language);

    $scope.setWidgetBlogPostText(language);
    $scope.setWidgetProfileText(language);
    $scope.setWidgetStatisticsText(language);
    $scope.setWidgetContactFormText(language);
  };

  $scope.$watch('currentLanguage', function(newValue, OldValue){
    $scope.setLanguage(newValue);
  });

  $scope.setHeaderText = function(language){
    LanguageService.getHeaderLanguageValues(language, function(err, data){
      if(!err){
        $rootScope.headerText = data;
        $rootScope.headerText['date'] = moment().format('MMM DD, YYYY');
      }
      else{
        console.error("Unable to get the footer values");
      }
    });
  };

  $scope.setGenericText = function(language){
    LanguageService.getGenericLanguageValues(language, function(err, data){
      if(!err){
        $rootScope.genericText = data;
        $rootScope.genericText['date'] = moment().format('MMM DD, YYYY');
      }
      else{
        console.error("Unable to get the footer values");
      }
    });
  };

  $scope.setFooterText = function(language){
  	LanguageService.getFooterLanguageValues(language, function(err, data){
  		if(!err){
	  		$rootScope.footerText = data;
		  	$rootScope.footerText['date'] = moment().format('MMM DD, YYYY');
  		}
  		else{
  			console.error("Unable to get the footer values");
  		}
  	});
  };

  $scope.setWidgetBlogPostText = function(language){
    LanguageService.getWidgetBlogPostData(language, function(err, data){
      if(!err){
        $rootScope.widgetBlogPostText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };

  $scope.setWidgetProfileText = function(language){
    LanguageService.getWidgetProfileData(language, function(err, data){
      if(!err){
        $rootScope.widgetProfileText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };

  $scope.setWidgetStatisticsText = function(language){
    LanguageService.getWidgetStatisticsData(language, function(err, data){
      if(!err){
        $rootScope.widgetStatisticsText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };

  $scope.setWidgetContactFormText = function(language){
    LanguageService.getWidgetContactFormData(language, function(err, data){
      if(!err){
        $rootScope.widgetContactFormText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };


});