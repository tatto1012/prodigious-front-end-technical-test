'use strict';

var conf = require('./gulp.conf.js');

var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var chalk = require('chalk');
var tap = require('gulp-tap');
var gutil = require('gulp-util');
var less = require('gulp-less');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var open = require('gulp-open');

var os = require('os');
var osName = require('os-name');


function getFileLoc(file){
  return require('path').relative( './', file.path);
}

/** Builds app.js and vendor.js */
gulp.task('build', ['build:js:vendor', 'build:js:app', 'build:checkSyntax', 'build:css'], function(){
  console.log("Complete the build process");
});

gulp.task('build:checkSyntax', function(){
  return gulp.src( conf.jsScripts[0] )
  .pipe(jshint())
  .pipe(tap( function(file){
    if(file.jshint.success){
      console.info( chalk.green( getFileLoc(file), 'Javascript Syntax OK!\n' ));
    }else{
      console.error( chalk.red( getFileLoc(file), 'Javascript Syntax Error!\n' ));
    }
  }))
  .pipe(jshint.reporter(stylish));
});

gulp.task('build:js:vendor', function(){
  return gulp.src( conf.vendorScripts )
  .pipe(tap(function(file){
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( conf.targetName.vendorJs ))
  .pipe(gulp.dest( conf.buildDirs.js ));
});

gulp.task('build:js:app', function(){
  return gulp.src( conf.jsScripts )
  .pipe(tap(function(file){
    // add file info to headers
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( conf.targetName.js))
  .pipe(gulp.dest(conf.buildDirs.js));
});

gulp.task('build:css', function(){
  return gulp.src( conf.lessAndCssFiles )
    // .pipe(size())
    .pipe(less())
    .pipe(rename(function(path){
      path.basename = conf.targetName.css;
    }))
    .pipe(concat( conf.targetName.css + '.css'))
    .pipe(gulp.dest(conf.buildDirs.css));
});

gulp.task('clean', function(){
  return gulp.src( [conf.buildDirs.js, conf.buildDirs.css], { read:false })
  .pipe(clean());
});

gulp.task('watch', ['build', 'webServer', 'openBrowser'], function(){
  console.log('Watching files for JS development');
  gulp.watch(conf.jsScripts, ['build']);
  gulp.watch(conf.vendorScripts, ['build']);
  gulp.watch(conf.lessWatchFiles, ['build']);
});


gulp.task('webServer', function() {
  connect.server({
    host : 'localhost',
    port : 8080
  });
});

gulp.task('openBrowser', function(){
  var options = {
    url: 'http://localhost:8080'
  };
  var operativeSystem = osName();

  if(operativeSystem.match(/OS X/i) ){
    options['app'] = 'google chrome';
  }
  else if(operativeSystem.match(/win/i) ){
    options['app'] = 'chrome';
  }
  else if(operativeSystem.match(/linux/i) ){
    options['app'] = 'google-chrome';
  }

  gulp.src('./index.html')
  .pipe(open('', options));
});

gulp.task('default', ['webServer', 'openBrowser']);