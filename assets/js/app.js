/* ====== application/scripts/app.js ===-_-=== */
// Initilize of the application and global access libraries

var app = angular.module('ProdigiousTechnicalTest', ['highcharts-ng', 'ngTagsInput']);
		app.value('_', '_');
		app.value('$', '$');
		app.value('moment', 'moment');
		app.value('randomColor', 'randomColor');

/* ====== application/scripts/controllers/languageSelector_Controller.js ===-_-=== */
app.controller('LanguageSelectorCtrl', function(LanguageService, $scope) {

	$scope.languages = {};

  $scope.getLanguageList = function(){
  	LanguageService.getLanguageList(function(err, data){
  		if(!err){
  			$scope.languages = data.languages[$scope.currentLanguage];
  		}
  		else{
  			console.error("Unable to get the language list");
  		}
  	});
  };

  $scope.$watch('currentLanguage', function(newValue, OldValue){
    $scope.getLanguageList();
  });

});
/* ====== application/scripts/controllers/main_Controller.js ===-_-=== */
app.controller('mainCtrl', function(LanguageService, $rootScope, $scope) {

  $rootScope.headerText  = {};
	$rootScope.footerText  = {};
  $rootScope.genericText = {};
  $rootScope.widgetBlogPostText     = {};
  $rootScope.widgetProfileText      = {};
  $rootScope.widgetStatisticsText   = {};
  $rootScope.widgetContactFormText  = {};

  $scope.setLanguage = function(language){
    $rootScope.currentLanguage = language;

    $scope.setHeaderText(language);
  	$scope.setGenericText(language);
    $scope.setFooterText(language);

    $scope.setWidgetBlogPostText(language);
    $scope.setWidgetProfileText(language);
    $scope.setWidgetStatisticsText(language);
    $scope.setWidgetContactFormText(language);
  };

  $scope.$watch('currentLanguage', function(newValue, OldValue){
    $scope.setLanguage(newValue);
  });

  $scope.setHeaderText = function(language){
    LanguageService.getHeaderLanguageValues(language, function(err, data){
      if(!err){
        $rootScope.headerText = data;
        $rootScope.headerText['date'] = moment().format('MMM DD, YYYY');
      }
      else{
        console.error("Unable to get the footer values");
      }
    });
  };

  $scope.setGenericText = function(language){
    LanguageService.getGenericLanguageValues(language, function(err, data){
      if(!err){
        $rootScope.genericText = data;
        $rootScope.genericText['date'] = moment().format('MMM DD, YYYY');
      }
      else{
        console.error("Unable to get the footer values");
      }
    });
  };

  $scope.setFooterText = function(language){
  	LanguageService.getFooterLanguageValues(language, function(err, data){
  		if(!err){
	  		$rootScope.footerText = data;
		  	$rootScope.footerText['date'] = moment().format('MMM DD, YYYY');
  		}
  		else{
  			console.error("Unable to get the footer values");
  		}
  	});
  };

  $scope.setWidgetBlogPostText = function(language){
    LanguageService.getWidgetBlogPostData(language, function(err, data){
      if(!err){
        $rootScope.widgetBlogPostText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };

  $scope.setWidgetProfileText = function(language){
    LanguageService.getWidgetProfileData(language, function(err, data){
      if(!err){
        $rootScope.widgetProfileText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };

  $scope.setWidgetStatisticsText = function(language){
    LanguageService.getWidgetStatisticsData(language, function(err, data){
      if(!err){
        $rootScope.widgetStatisticsText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };

  $scope.setWidgetContactFormText = function(language){
    LanguageService.getWidgetContactFormData(language, function(err, data){
      if(!err){
        $rootScope.widgetContactFormText = data;
      }
      else{
        console.error("Unable to get the widget Blog Post values");
      }
    });
  };


});
/* ====== application/scripts/controllers/siteBody_Controller.js ===-_-=== */
app.controller('SiteBodyCtrl', function(LanguageService, $rootScope, $scope) {

});
/* ====== application/scripts/controllers/siteFooter_Controller.js ===-_-=== */
app.controller('SiteFooterCtrl', function(LanguageService, $scope) {

});
/* ====== application/scripts/controllers/siteHeader_Controller.js ===-_-=== */
app.controller('SiteHeaderCtrl', function(LanguageService, $scope) {
	
});
/* ====== application/scripts/controllers/widgeContactForm_Controller.js ===-_-=== */
app.controller('WidgetContactFormCtrl', function(LanguageService, $rootScope, $scope, $timeout, $window) {

	$scope.widgetData = $rootScope.widgetContactFormText.form;

  $scope.$watch('$root.widgetContactFormText', function(newValue){
    $scope.widgetData = newValue.form;
  },true);

	$scope.errors = {};
	$scope.showStatusMessage = false;
	$scope.saveFormstatus = false;
	$scope.notifyStyle = "danger";
	$scope.notifyMesage = "";
	$scope.contactForm = {
		subject : '',
		message : ''
	};
	$scope.emails  = [
		'jhon@doe.com',
		'jhon+1@doe.com'
	];


  $scope.setColoursLabels = function(){
  	_.each($(".tagsInputsStyles .tags .tag-item"), function(item,key){
    	var color = randomColor();
    	if( !$(item).hasClass('colourApplied') ){
    		$(item).css({ "background" : color, "border-color" : color}).addClass('colourApplied');
    	}
    });
  };


  $scope.sendEmail = function(){
  	$scope.showStatusMessage = false;

  	if(!$scope.emails || _.isEmpty($scope.emails) || $scope.emails.length == 0){
  		$scope.errors['email'] = $scope.widgetData.errorCases.email;
  	}

  	if(!$scope.contactForm.subject.$valid){
  		$scope.errors['subject'] = $scope.widgetData.errorCases.subject;
  	}

  	if(!$scope.contactForm.message.$valid){
  		$scope.errors['message'] = $scope.widgetData.errorCases.message;
  	}

  	if($scope.contactForm.$valid){
  		$scope.showStatusMessage = true;
  		$scope.notifyStyle = "success";
  		$scope.notifyMesage = $scope.widgetData.messages.success;

  		$timeout(function(){
  			$scope.resetForm();
  		},2000);
  	}
  	else{
  		$scope.showStatusMessage = true;
  		$scope.notifyStyle = "danger";
  		$scope.notifyMesage = $scope.widgetData.messages.error;
  	}

  };

  $scope.resetError = function(item){
  	$scope.errors[item] = false;
  	$scope.showStatusMessage = false;
  };

  $scope.resetForm = function(){
  	$scope.showStatusMessage = false;
  	$scope.contactForm = {
			subject : '',
			subjectValue : '',
			message : '',
			messageValue : ''
		};
		$scope.emails = [];
  };

  $scope.saveStatus = function(){
  	$scope.showStatusMessage = true;
  	if($scope.saveFormstatus){
  		var contactFormValues = {
  			emails : _.pluck($scope.emails,'text'),
  			subjectValue : $scope.contactForm.subject['$modelValue'],
  			messageValue : $scope.contactForm.message['$modelValue']
  		};
  		$window.localStorage.setItem("formSaved", JSON.stringify(contactFormValues));
  		$scope.notifyStyle = "info";
  		$scope.notifyMesage = $scope.widgetData.messages.info;

  		$timeout(function(){
  			$scope.showStatusMessage = false;
  		},2000);
  	}
  	else{
  		$scope.notifyStyle = "warning";
  		$scope.notifyMesage = $scope.widgetData.messages.warning;

  		$window.localStorage.removeItem("formSaved");

  		$timeout(function(){
  			$scope.resetForm();
  			$scope.showStatusMessage = false;
  		},2000);
  	}
  };

  $scope.refillContactForm = function(){
  	if($window.localStorage.getItem("formSaved")){
  		var formData = JSON.parse($window.localStorage.getItem("formSaved"));
  		$scope.saveFormstatus = true;

  		_.each(formData, function(itemValue, key){
  			if(key == 'emails'){
  				$scope[key] = itemValue;
  			}
  			else{
  				$scope.contactForm[key] = itemValue;
  			}
  		});
  	}
  	else{
  		$scope.saveFormstatus = false;
  	}
  }

  $scope.$watch('emails.length', function(value) {
    $scope.resetError('email');
    $scope.setColoursLabels();
  });

  $timeout(function(){
  	$scope.setColoursLabels();
  	$scope.refillContactForm();
  }, 1000);

});
/* ====== application/scripts/controllers/widgetBlogPost_Controller.js ===-_-=== */
app.controller('WidgetBlogPostCtrl', function(LanguageService, $rootScope, $scope) {

	$scope.widgetData = $rootScope.widgetBlogPostText;

	$scope.$watch('$root.widgetBlogPostText', function(newValue){
    $scope.widgetData = newValue;
  },true);

});
/* ====== application/scripts/controllers/widgetProfile_Controller.js ===-_-=== */
app.controller('WidgetProfileCtrl', function(LanguageService, $rootScope, $scope) {

	$scope.widgetData = $rootScope.widgetProfileText;

	$scope.$watch('$root.widgetProfileText', function(newValue){
    $scope.widgetData = newValue;
  },true);

	$scope.highlightToogle= function(item, status){
		var color = (status)? '#e55f3b' : '#c6bfb8' ;
		$(".fa-"+item).css({ 'color' : color });

		if(status){
			$(".fa-"+item).closest('.btn').addClass('separator');
		}
		else{
			$(".fa-"+item).closest('.btn').removeClass('separator');
		}
	}

});
/* ====== application/scripts/controllers/widgetStatistics_Controller.js ===-_-=== */
app.controller('WidgetStatisticsCtrl', function(LanguageService, $rootScope, $scope, $timeout) {

	$scope.showDiagramStats  = true;
	$scope.showMonthlyReport = false;

  $scope.widgetData = $rootScope.widgetStatisticsText;

  $scope.$watch('$root.widgetStatisticsText', function(newValue){
    $scope.widgetData = newValue;
    $scope.setChartsData();
  },true);

  $scope.showTab = function(tabItem){
    var tabs = ['showDiagramStats', 'showMonthlyReport'];

    _.each(tabs, function(item, key){
      $scope[item] = false;
    });

    switch(tabItem){
      case 'showDiagramStats':
          $scope[tabItem] = true;
        break;
      case 'showMonthlyReport':
          $scope[tabItem] = true;
        break;
    };

  };

  $scope.DTchartConfig = {
    options : {
      plotOptions: {
        pie: {
          innerSize : 130,
          allowPointSelect: true,
          cursor: 'pointer',
          showInLegend: true,
          slicedOffset: 0,
          depth : 45

        }
      },
      legend: {
        enabled : false
      }
    },
    title: {
      text: ''
    },
    series: [{
      type: 'pie',
      name: '',
      data: [],
      dataLabels: {
        enabled: false
      }
    }],
    tooltip : {
      enabled :false
    },
    func: function chartMiddlevalues(chart) {

      var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
      var textY = chart.plotTop  + (chart.plotHeight * 0.5);

      var filesAmount = ($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[0])? $scope.widgetData.charts[0].filesAmount : '-';
      var filesGB = ($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[0])? $scope.widgetData.charts[0].filesGB : '-';

      var content = '<span id="pieChartInfoText" style="position:absolute; text-align:center;width:85px;">';
      content += '<span style="font-size:16px;color:red;font-weight:bold;">'+ filesAmount +'</span><br>';
      content += '<span style="font-size: 33px;color:gray;font-weight:bold;">'+ filesGB +'</span>';
      content += '</span>';

      $("#addText").append(content);
      var span = $('#pieChartInfoText');
      $('#pieChartInfoText').css({
        'left' : textX + (span.width() * -0.3),
        'top'  : textY + (span.height() * -0.5)
      });

    }

  };


  $scope.MDTconfig = {
    title: {
      text: ''
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: { text: '' }
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
      borderWidth: 0
    },
    series: []
  };

  $scope.setChartsData = function(){

    if($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[0]){
      $scope.DTchartConfig.title.text = $scope.widgetData.charts[0].title;
      $scope.DTchartConfig.series[0].name = $scope.widgetData.charts[0].label;
      $scope.DTchartConfig.series[0].data = $scope.widgetData.charts[0].data;
    }

    if($scope.widgetData && $scope.widgetData.charts && $scope.widgetData.charts[1]){
      $scope.MDTconfig.title.text = $scope.widgetData.charts[1].title;
      $scope.MDTconfig.categories = $scope.widgetData.charts[1].categories;
      $scope.MDTconfig.yAxis.title.text = $scope.widgetData.charts[1].label;
      $scope.MDTconfig.series = $scope.widgetData.charts[1].data
    }

  }

  $timeout(function() {
    $scope.setChartsData();
  }, 1000);


});
/* ====== application/scripts/directives/animateNumber_Directive.js ===-_-=== */
/**
 * animateNumber
 * @author:         Jonathan Rodriguez
 */
app.directive( 'animateNumber', function(appHelper){

  return {

    restrict: 'E',
    replace: true,
    template : '<span id="followers-counter"></span>',
    link : function(scope, element, attr){

      var decimalPlaces = (attr.decimalPlaces && attr.decimalPlaces != '')? attr.decimalPlaces : 2;
    	var initValue = (attr.initialValue && attr.initialValue != '')? attr.initialValue : 0;
    	var endValue  = (attr.endValue && attr.endValue != '')? attr.endValue : 10;

      if(decimalPlaces == '0'){
        $(element).prop('number', initValue)
        .animateNumber({ number: endValue }, 5000 );
      }
      else{
      	$(element).prop('number', initValue).animateNumber({ number: endValue, numberStep: function(now, tween) {

          var target = $(tween.elem);

          if (decimalPlaces > 0) {
            now = now.toFixed(decimalPlaces);
            now = now.toString().replace('.', ',');
          }

          target.text(now);

        }}, 3000);
      }


    }

  };

});
/* ====== application/scripts/directives/inlineErrors_Directive.js ===-_-=== */
app.directive( 'inlineError', function(appHelper){

  	return {

    	restrict: 'E',
    	templateUrl: appHelper.templatePath('features/inline-errors'),
    	scope:{
    		errors: '=errors'
    	}

  	}

});
/* ====== application/scripts/directives/languajeSelector_Directive.js ===-_-=== */
/**
 * languageSelector
 * @author:         Jonathan Rodriguez
 */
app.directive( 'languageSelector', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'LanguageSelectorCtrl',
    templateUrl: appHelper.templatePath('header-content/language-selector'),
    scope:{
      currentLanguage : "=currentLanguage"
    }

  }

});
/* ====== application/scripts/directives/siteBody.Directive.js ===-_-=== */
/**
 * siteBody
 * @author:         Jonathan Rodriguez
 */
app.directive( 'siteBody', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'SiteBodyCtrl',
    templateUrl: appHelper.templatePath('layout/body'),
    scope:{

    }

  }

});
/* ====== application/scripts/directives/siteFooter_Directive.js ===-_-=== */
/**
 * siteFooter
 * @author:         Jonathan Rodriguez
 */
app.directive( 'siteFooter', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'SiteFooterCtrl',
    templateUrl: appHelper.templatePath('layout/footer'),
    scope:{

    }

  }

});
/* ====== application/scripts/directives/siteHeader_Directive.js ===-_-=== */
/**
 * siteHeader
 * @author:         Jonathan Rodriguez
 */
app.directive( 'siteHeader', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'SiteHeaderCtrl',
    templateUrl: appHelper.templatePath('layout/header'),
    scope:{
      currentLanguage : "=currentLanguage"
    }

  }

});
/* ====== application/scripts/directives/widgetBlogPost_Directive.js ===-_-=== */
/**
 * widgetBlogPost
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetBlogPost', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetBlogPostCtrl',
    templateUrl: appHelper.templatePath('widgets/blogPost'),

  }

});
/* ====== application/scripts/directives/widgetContactForm_Directive.js ===-_-=== */
/**
 * widgetContactForm
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetContactForm', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetContactFormCtrl',
    templateUrl: appHelper.templatePath('widgets/contactForm'),
    scope:{

    }

  }

});
/* ====== application/scripts/directives/widgetProfile_Directive.js ===-_-=== */
/**
 * widgetProfile
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetProfile', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetProfileCtrl',
    templateUrl: appHelper.templatePath('widgets/profile'),
    scope:{

    }

  }

});
/* ====== application/scripts/directives/widgetStatistics_Directive.js ===-_-=== */
/**
 * widgetStatistics
 * @author:         Jonathan Rodriguez
 */
app.directive( 'widgetStatistics', function(appHelper){

  return {

    restrict: 'E',
    replace: false,
    controller : 'WidgetStatisticsCtrl',
    templateUrl: appHelper.templatePath('widgets/statistics'),
    scope:{

    }

  }

});
/* ====== application/scripts/services/appHelper.js ===-_-=== */
'use strict';

app.constant('appHelper', {

  templatesDir: 'application/templates',

  templatePath: function(view_name){
    return this.templatesDir + '/' + view_name + '.html';
  }

});
/* ====== application/scripts/services/languageService.js ===-_-=== */
'use strict';

app.factory('LanguageService', function ($http, $location) {

  return {
    getLanguageList : getLanguageList,
    getHeaderLanguageValues : getHeaderLanguageValues,
    getGenericLanguageValues : getGenericLanguageValues,
    getFooterLanguageValues : getFooterLanguageValues,
    getWidgetBlogPostData : getWidgetBlogPostData,
    getWidgetProfileData : getWidgetProfileData,
    getWidgetContactFormData : getWidgetContactFormData,
    getWidgetStatisticsData : getWidgetStatisticsData
  };

  function getLanguageList(callback){
    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/availableLanguages.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });
  };

  function getHeaderLanguageValues(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/header.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getGenericLanguageValues(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/generic.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getFooterLanguageValues(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/footer.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetBlogPostData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetBlogPost.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetProfileData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetProfile.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetStatisticsData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetStatistics.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

  function getWidgetContactFormData(language, callback){

    $http({
      method:'GET',
      url: $location.absUrl() +'application/languages/' + language + '/widgetContactForm.json',
      data: {}
    })
    .success(function(response){
      callback(null, response);
    })
    .error(function(error){
      callback(error);
    });

  };

});
